<!-- File: /app/View/Posts/view.ctp -->

<h1><b><?php echo h($post['Post']['title']); ?></b></h1>
<p>
<center> 
    <?php
        if ($post['Post']['image_url'] != null)
            echo $this->Html->image( $post['Post']['image_url'], array( 'width'=>'400px', 'height'=>'300px')); ?> 
</center>
</p>

<p><large><?php echo h($post['Post']['body']); ?></large></p>
<p><large>Created: <?php echo $post['Post']['created']; ?></large></p>

<table>
    <tr><td > Image </td>
        <td>Username </td> 
		<td> Comments </td>
		<td> Actions </td>
	</tr>

	<?php foreach ($post['Comment'] as $comment): ?>

	<tr> <td>  
        <?php  
            if($comment['image'] != null)
                echo $this->Html->image( $comment['image'], array( 'width'=>'20px', 'height'=>'20px'));
        ?> </td> 
        
		<td>  <?php echo h($comment['comment_user']); ?> </td>
		<td>  <?php echo h($comment['comments']); ?>  </td>
		<td>              
          <?php
            if ($comment['comment_user'] == $username) {
                echo $this->Html->link(
                    'Delete', array('controller'=>'comments','action' => 'delete', $comment['id'], $comment['post_id'])
                );
                echo $this->Html->link(
                    'Edit', array('controller'=>'comments','action' => 'edit', $comment['id'], $comment['post_id'])
                );
        
            }
            ?>
        </td>
        </tr>
		<?php endforeach; ?>
</table>

<p>
<?php echo $this->Form->create('Comment', array('enctype'=>"multipart/form-data",'url'=>array('controller'=>'posts','action'=>'view',$post['Post']['id']))); ?>
      
    <fieldset>
        <legend><?php __('Add Comment');?></legend>
    <?php
        echo $this->Form->input('Comment.comments');
    ?>
    <?php echo $this->Form->input('image',array("type"=>"file","size"=>"45", 'error' => false,'placeholder'=>'Upload Image')); ?>
    </fieldset>

 <?php echo $this->Form->end('Add comment');?>
 </p>
