<h1> <b> Search Blog </b></h1>
<table> 

<?php echo $this->Form->create('Post',array('controller'=>'posts','url'=>'Search')); ?> 
<tr> 
<td><?php echo $this->Form->input('Search by Id',array('type'=>'text' ,'rows' => '1')); ?> </td>
<td><?php echo $this->Form->input('Search by Title',array('type'=>'text','rows' => '1')); ?> </td> 
<td><?php echo $this->Form->input('Search by Body',array('type'=>'text','rows' => '1')); ?> </td>
</tr>
</table>
<?php echo $this->Form->end('Search');?>
<h1> Blog posts </h1>
<p><?php echo $this->Html->link('Add Post', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th> 
        <th>Title</th>
        <th>Actions</th>
        <th>Created</th>
        
        
    </tr>
<!-- Here's where we loop through our $posts array, printing out post info -->
    <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>

            <?php
            if ($post['Post']['user_id']== $userid) {
                
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['id']),
                    array('confirm' => 'Are you sure?')
                );
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $post['Post']['id'])
                );
            }
            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
        
    </tr>
    <?php endforeach; ?>

</table>
<p><?php echo $this->Html->link('Logout' ,array('controller'=>'users','action'=>'logout')); ?></p>