<?php

class PostsController extends AppController {
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash');

    public function index() {
        $this->set('posts', $this->Post->find('all'));
        $id = $this->Auth->user('id');
        $this->set('userid', $id);
        /*$this->Paginator->settings = array('Post' => array('paramType' => 'querystring','limit' => 5, 'older' => array('Post.id' => 'asc')));
        $this -> set ('posts', $this->Paginator -> paginator());*/
    }

    public function view($id=null) {

        $username = $this->Auth->user('username');
        $this->set('username',$username);       
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
 
        if (!empty($this->request->data['Comment'])) {
                $this->request->data['Comment']['comment_user']= $this->Auth->user('username');
                $this->request->data['Comment']['post_id'] = $id; 
                $this->Post->Comment->create(); 

            if(!empty($this->data['Comment']['image']['name']))
             {
                $file = $this->data['Comment']['image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if(in_array($ext, $arr_ext))
                {

                if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . DS . $file['name']))
                 {
                $this->request->data['Comment']['image'] = '/img/' .$file['name'];
               // pr($this->data);
                    }  
                } 
            }  
             
                if ($this->Post->Comment->save($this->data)) {
                $this->Flash->success(__('The Comment has been saved.', true),'success');
                $this->redirect(array('action'=>'view',$id));
                }
                $this->Flash->success(__('The Comment could not be saved. Please, try again.', true),'warning');
        }
            
            $post = $this->Post->read(null, $id); // contains $post['Comments']
            $this->set(compact('post'));
         
        
    }

    public function add() {
        if ($this->request->is('post')) {
            if(!empty($this->data['Post']['photo']['name']))
             {
                $file = $this->data['Post']['photo']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if(in_array($ext, $arr_ext))
                {
                    if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . DS . $file['name']))
                    {
                        $this->request->data['Post']['image_url'] = '/img/' .$file['name'];
                     }  
                }            
            }

           // $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }
    }

    public function edit($id = null) {
        
        if (!$id) {
        throw new NotFoundException(__('Invalid post'));
        }
        //if ($post[$user_id]== $this->Auth->user('id'))

            $post = $this->Post->findById($id);
            if (!$post) {
            throw new NotFoundException(__('Invalid post'));
            }

            if ($this->request->is(array('post', 'put'))) {
             $this->Post->id = $id;
             //
                 if(!empty($this->data['Post']['Edit_Photo']['name']))
                 {
                    $file = $this->data['Post']['Edit_Photo']; //put the  data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    if(in_array($ext, $arr_ext))
                    {
                        if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . DS . $file['name']))
                        {
                        $this->request->data['Post']['image_url'] = '/img/' .$file['name'];
                        }  
                    }            
                }
            //
             if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
                }
                $this->Flash->error(__('Unable to update your post.'));
            }

            if (!$this->request->data) {
            $this->request->data = $post;
            }       
    } 

    public function delete($id) {
        if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
        $this->Flash->success(
            __('The post with id: %s has been deleted.', h($id))
        );
        } else {
        $this->Flash->error(
            __('The post with id: %s could not be deleted.', h($id))
        );
        }

        return $this->redirect(array('action' => 'index'));
    }

    public function search() {
        $id = $this->request->data['Post']['Search by Id'];
        $title = $this->request->data['Post']['Search by Title'];
        $body = $this->request->data['Post']['Search by Body'];
        
        if ($id == "" && $title == "" && $body == "") {
        $this->Flash->error(__('Please fill you want to search'));
        return $this->redirect(array('action' => 'index'));
        }

        else if ($id !="" && $title == "" && $body == "") {
        $results = $this->Post->find('all', array(
        'conditions' => array('Post.id LIKE' => "%". $id ."%")));
        //return $this->redirect(array('action'=> 'search'));
        }

        else if ($id == "" && $title != "" && $body == "") {
          $results = $this->Post->find('all', array(
          'conditions' => array('Post.title LIKE' => "%". $title ."%")));       
        }

        else if ($id == "" && $title == "" && $body != "") {
           $results = $this->Post->find('all', array(
          'conditions' => array('Post.body LIKE' => "%". $id ."%")));
        }
        $this->set('results',$results);
    }

    public function isAuthorized($user) {
    // All registered users can add posts
    if ($this->action === 'add') {
        return true;
    }

    // The owner of a post can edit and delete it
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Post->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
    }
    
}
