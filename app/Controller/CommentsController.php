<?php

class CommentsController extends AppController {
public $helpers = array('Html', 'Form', 'Flash');
public $components = array('Flash');

	public function edit($id = null, $post_id=null) {
        $comment = $this->Comment->findById($id);
        if (!$comment) {
           throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('comment', 'put'))) {
            $this->Comment->id = $id;
            if ($this->Comment->save($this->request->data)) {            	
            	$this->Flash->success(__('Your comment has been updated.'));
            	return $this->redirect(array('controller'=>'posts','action' => 'view', $post_id));
            }
            $this->Flash->error(__('Unable to update your comment.'));
        }
            if (!$this->request->data) {
            $this->request->data = $comment;
            }       
    } 
    public function delete($id, $post_id) {
       
        if ($this->Comment->delete($id)) {
        	$this->Flash->success(
            __('The post with id: %s has been deleted.', h($id))
        );
        }  else {
        	$this->Flash->error(
            __('The post with id: %s could not be deleted.', h($id))
        );
        }
       return $this->redirect(array('controller'=>'posts','action' => 'view/'.$post_id));
    }
}
