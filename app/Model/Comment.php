<?php
class Comment extends AppModel {
    var $name = 'Comment';
    public $validate = array(
        'title' => array(
            'rule' => 'notBlank'
        ),
        'body' => array(
            'rule' => 'notBlank'
        )
    );
}